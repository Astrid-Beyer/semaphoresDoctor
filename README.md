# Projet de c++ sur les sémaphores

Réalisé à partir d'un exemple dans The Little Book of Semaphores (Allen B. Downey)
http://greenteapress.com/semaphores/LittleBookOfSemaphores.pdf

#### Problème reprit de la vie réelle en s'inspirant du Barbershop Problem du Little Book of Semaphores (page 121) :

Un médecin offre des masques de protection contre le Covid-19 à ses patients.
Sachant cela, la ville où se situe son cabinet est devenue complètement hystérique et le docteur a du mal à gérer le trafic de patients entrants dans son cabinet.
Il ne doit pas y avoir plus de 3 patients dans l'enceinte de la salle d'attente et de 1 dans son bureau.

---

Cabinet avec N chaises d'attente et la salle du médecin avec son bureau (qui est une chaise techniquement)

* si il n'y a pas de patients à soigner le médecin regarde la télé
* si un patient entre en salle d'attente et que toutes les chaises sont occupées le patient quitte le bâtiment
* si le médecin est occupé mais les chaises dispo le patient s'assoit sur une chaise libre 
* si le docteur regarde la télé le patient le 'secoue'

---
## Pour compiler

`g++ -pthread "chemin/semaphoreDoctor.cpp" && ./a.out [nbreDePatients]`  

`g++ -pthread "chemin/hoarDoctor.cpp" && ./a.out [nbreDePatients]`

Où nbreDePatients doit être un nombre réel. Je recommande de tester avec au moins 5, pour voir le cas de figure où un patient doit quitter le bâtiment parce qu'il n'y a plus de chaises disponibles.