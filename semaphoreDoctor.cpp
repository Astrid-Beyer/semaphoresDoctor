/**
 *
 * @file    semaphoreDoctor.cpp
 *
 * @author  Astrid Beyer
 *
 * @date    04/04/2020
 *
 * @version 1.0
 *
 * @brief   Mise en situation d'une journée type dans un cabinet de médecin
 * 
 **/

#include <iostream>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>

using namespace std;

#define N 4 // N est le nbre total de patients qui peuvent être dans la salle d'attente : 3 en salle et 1 dans le bureau (N est une constante globale)

int   patients = 0; // patients compte le nombre de patients dans la salle, protégé par mutex
bool  endOfDay = 0; // fin de journée du docteur (false de base)
sem_t mutex;
sem_t semPatient;
sem_t semDoctor;
sem_t happyPatient;
sem_t doctorDone;

/* Prototype des fonctions */
void *patient(void * num); 
void *doctor(void *);
void  grumble();             // fonction pour que les clients puissent grogner, ne retourne rien
void  randomWait(int param); // fonction pour faire patienter entre les actions
void  printDoctor(string text);                      // couleur de texte pour le docteur

int main(int argc, char * argv[]) {
    printf("\033[1;35m"); // texte en gras magenta
    printf("Un médecin offre des masques de protection contre le Covid-19 à ses patients.\nSachant cela, la ville où se situe son cabinet est devenue complètement hystérique"
    " et le docteur a du mal à gérer le trafic de patients entrants dans son cabinet. Afin de respecter les distances de sécurité, il ne doit pas y avoir plus de 3 patients dans l'enceinte de la salle d'attente"
    " et de 1 dans son bureau.\n\n\n");
    printf("\033[0m");    // réinitalise la couleur
     
    
    int numPatients = atoi(argv[1]); // nbre de patients qui peuvent arriver

    //initialisation des sémaphores
    sem_init(&mutex,      0, 1); // mutex (Semaphore(1))             => protection pour que les variables ne soient pas modifiées en même temps
    sem_init(&semPatient, 0, 0); // patient (Semaphore(0))           => nbre de patients actuellement dans le cabinet
    sem_init(&semDoctor,  0, 0); // doctor (Semaphore(0))            => place dans le cabinet du docteur
    sem_init(&happyPatient, 0, 0); // happyPatient (Semaphore(0))    => lorsque le docteur s'occupe du patient
    sem_init(&doctorDone,   0, 0); // doctorDone (Semaphore(0))      => lorsque le docteur a donné un masque

    pthread_t doctorThread;      // thread qui exécute le code du médecin
    pthread_t patientThread [numPatients]; // thread qui exécute le code du patient

    /* ---  Thread médecin  --- */
    pthread_create(&doctorThread, NULL, doctor, NULL);
    for (int i = 0; i < numPatients; ++i) { // pour chaque patients...
        pthread_create(&patientThread[i], NULL, patient, (void*)& i); // ...on lance le thread de patients
    }
    for (int i = 0; i < numPatients; ++i) { // pour chaque patients...
        pthread_join(patientThread[i], NULL); // ...on attend que leurs threads se finissent
    }
    // une fois que tous les patients ont été vus par le docteur, la journée du médecin est finie
    endOfDay = 1;
    sem_post(&semPatient);
    pthread_join(doctorThread, NULL);

    return 0;
} // int main()

void growl(int num) {
    printf("Le patient numéro %d n'a pas pu rentrer. Il rentre chez lui en râlant.\n", num);
} // growl()

void *patient(void * num) {
    int number = *(int *)num; // on change le type de la variable num en type nombre réel

    randomWait(50); // Temps avant qu'un patient ne parte de chez lui, pour ne pas qu'ils partent tous en même temps
    printf("Le patient numéro %d part de chez lui.\n", number);
    
    randomWait(50); // Le temps que le patient fait le chemin de chez lui à chez le docteur
    printf("Le patient numéro %d arrive chez le docteur.\n", number);
    
    sem_wait(&mutex);
    if (patients == N) {
        sem_post(&mutex);
        growl(number);
        return NULL;
    }
    patients += 1;

    printf("Le patient numéro %d s'installe dans la salle d'attente du docteur.\n", number);
    sem_post(&mutex);

    sem_post(&semPatient); // le patient arrive dans la salle et se signale...
    sem_wait(&semDoctor);  // ...il attend que le docteur le prenne en charge

    printf("Le patient numéro %d rentre dans le bureau du docteur.\n", number);

    randomWait(8); // en attendant que le patient s'installe dans le bureau du docteur

    sem_post(&happyPatient); // signale que le docteur s'occupe du patient

    printf( "Le patient numéro %d attend que le docteur ait fini.\n", number);

    sem_wait(&doctorDone);   // et il attend que le docteur ait fini de s'occuper de lui

    sem_wait(&mutex); // attend que la ressource patient soit disponible d'accès...
    patients -= 1;    // ...la modifie...
    printf("Le patient numéro %d rentre heureux, avec son masque, chez lui.\n", number);
    sem_post(&mutex); // ... et la rend disponible pour les autres objets qui voudront utiliser la ressource (variable patients)


} // void patient()

void *doctor(void *) {
    while (endOfDay == 0) { // tant que la journée n'est pas finie
        if(patients == 0)  {
            printDoctor("Le docteur regarde la télé en attendant les prochains patients.\n");
        }
        sem_wait(&semPatient); // le docteur attend qu'un patient arrive dans la salle...
        if (endOfDay == 0) {
            sem_post(&semDoctor);  // ...il s'occupe d'un patient (réserve la place dans son bureau)

            printDoctor("Le docteur invite le patient suivant à rentrer.\n");
            sem_wait(&happyPatient); // le docteur attend que le patient soit bien installé...

            printDoctor("Le docteur regarde dans sa boîte de masque.\n");
            randomWait(5); // Le temps que le docteur fouille dans la boite et donne le masque

            printDoctor("Le docteur donne son masque au patient.\n");
            sem_post(&doctorDone);   // ... et il finit de s'en occuper

            printDoctor("Le docteur prépare le prochain rendez-vous.\n");
            randomWait(5);
        }
    }
    printDoctor("Le docteur a terminé sa journée ! Il rentre chez lui à 20h, sous les applaudissements.\n");

} // void doctor()

void randomWait(int param) {
    int randomTime = rand() % param + 1;
    sleep(randomTime);
    printf("\033[0;33m");
    if (randomTime == 1) {
        printf("\nLe temps qui s'est écoulé est de : %d seconde. C'était rapide !\n\n", randomTime);
    } else
        printf("\nLe temps qui s'est écoulé est de : %d seconde(s).\n\n", randomTime);
    printf("\033[0m");
} // void randomWait()

void printDoctor(string text) {
    printf("\033[0;31m");       // rouge
    printf("%s", text.c_str()); // passer l'objet string c++ en objet string c
    printf("\033[0m");          // réinitialise la couleur
} // printDoctor()