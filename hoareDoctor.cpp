/**
 *
 * @file    hoareDoctor.cpp
 *
 * @author  Astrid Beyer, Adem Louriachi
 *
 * @date    04/04/2020
 *
 * @version 1.0
 *
 * @brief   Mise en situation d'une journée type dans un cabinet de médecin en utilisant les moniteurs de Hoare
 * 
 **/

#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <unistd.h>

using namespace std;

#define N 4

/* --- Classe monitor (cabinet du médecin) --- */ 
class hoareDoctor {
    private:
        int chairAvailable = 4; // nombre de chaises disponibles dans le cabinet
        condition_variable doctorAvailable;  // si le médecin est disponible
        condition_variable patientAvailable; // si le patient est disponible
        condition_variable doctorDone; // si le médecin à fini
        mutex mutexDoctor;  // création de mutex pour le médecin
        mutex mutexPatient; // création de mutex pour le patient
        mutex mutexDoctorDone; //création de mutex pour savoir si le travail du médecin

    public:
        void patient(int number) {
            printf("Le patient numéro %d part de chez lui.\n", number);
            this->randomWait();
            printf("Le patient %d arrive chez le docteur.\n", number);
            if (chairAvailable > 0) {                     // si il reste des places...
                chairAvailable -= 1;                      // ...le patient occupe une place...
                printf("Le patient numéro %d s'installe dans la salle d'attente du docteur.\n", number);
                printf("Il reste %d places...\n", chairAvailable);
                patientAvailable.notify_one();              // ...le patient signale sa présence au docteur...
                unique_lock<mutex> lkDoctor(mutexDoctor);   // ...on définit un verrou pour le mutex médecin...
                doctorAvailable.wait(lkDoctor);             // ...le patient attend que le médecin soit disponible (en utilisant le verrou)
                printf("Le patient numéro %d rentre dans le bureau du docteur.\n", number);
                unique_lock<mutex> lkDone(mutexDoctorDone); // ...on définit un verrou pour le mutex du travail du médecin...
                doctorDone.wait(lkDone);                    // ...le patient attends que le medecin est fini...
                printf("Le patient numéro %d rentre heureux, avec son masque, chez lui.\n", number);
            } else {
                printf("Le patient numéro %d n'a pas pu rentrer. Il rentre chez lui en râlant.\n", number);
            }
        } // void patient()

        void doctor(bool * endOfDay) {
            while(*endOfDay == 0) {
                while (chairAvailable == N && *endOfDay == 0) {
                    printf("Le docteur regarde la télé en attendant les prochains patients.\n");
                    unique_lock<mutex> lkPatient(mutexPatient); // on définit un verrou pour le mutex patient
                    patientAvailable.wait(lkPatient);           // le médecin attend que le patient entre dans son bureau (en utilisant le verrou)
                }
                if(*endOfDay == 0) {
                    printf("Le docteur invite le patient suivant à rentrer\n");
                    doctorAvailable.notify_one(); // le médecin prend en charge le patient suivant
                    this->randomWait();
                    printf("Le docteur regarde dans sa boîte de masque.\n");
                    this->randomWait();
                    doctorDone.notify_one();
                    printf("Le docteur donne son masque au patient.\n");
                    chairAvailable += 1;          // le patient libère une place
                    this->randomWait();
                }
            }
            printf("Le docteur a terminé sa journée ! Il rentre chez lui à 20h, sous les applaudissements.\n");
        } // void doctor()

        void randomWait() {
            int randomTime = rand() % 5 + 1;
            sleep(randomTime);
        } // void randomWait()

        void unlockDoctorEndOfDay(){
            patientAvailable.notify_one();
        } // void unlockDoctorEndOfDay()

}; // class hoareDoctor()


int main (int argc, char * argv[]) {
    printf("Un médecin offre des masques de protection contre le Covid-19 à ses patients.\nSachant cela, la ville où se situe son cabinet est devenue complètement hystérique");
    printf(" et le docteur a du mal à gérer le trafic de patients entrants dans son cabinet. Il ne doit pas y avoir plus de 3 patients dans l'enceinte de la salle d'attente");
    printf(" et de 1 dans son bureau.\n\n\n");

    hoareDoctor * hoareDocPointer = new hoareDoctor(); // on crée un pointeur vers un nouvel objet hoareDoctor
    
    bool endOfDay = 0; // fin de journée du docteur (false de base)

    thread doctorThread(&hoareDoctor::doctor, hoareDocPointer, &endOfDay); // thread du médecin de l'objet hoareDoctor

    vector<thread> patientsThreads; // liste de thread de patients
    for (int i=1; i<=atoi(argv[1]); ++i) { // pour chaques patients...
        patientsThreads.push_back(thread(&hoareDoctor::patient, hoareDocPointer, i)); // ...on crée un thread du patient de l'objet hoareDoctor
    }
    for (auto& th : patientsThreads) th.join(); // pour chaque patients on attend que leurs threads se finissent

    endOfDay = 1;
    hoareDocPointer->unlockDoctorEndOfDay(); // on déverouille le docteur pour qu'il termine sa journée (et arrête d'attendre des patients)
    doctorThread.join(); // on attend que le docteur ait fini
    return 0;
} // int main()